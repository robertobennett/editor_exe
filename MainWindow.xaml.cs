﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Interop;
using System.Runtime.InteropServices;
using Editor_Wrapper;

namespace Editor_Exe
{
    public class CRenderTarget : HwndHost
    {
        public delegate IntPtr WndProcDelegate(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam);
        private WndProcDelegate m_WndProc = null;
        public WndProcDelegate WndProcPointer
        {
            set
            {
                m_WndProc = value;
            }
        }
        private IntPtr m_hWnd = IntPtr.Zero;
        public IntPtr hWnd
        {
            get
            {
                return m_hWnd;
            }
        }
        private int m_iHeight, m_iWidth;
        const uint
            WS_CHILD = 0x40000000,
            WS_VISIBLE = 0x10000000,
            WS_POPUP = 0x80000000,
            WS_OVERLAPPED = 0x00000000;

        [DllImport("User32.dll")]
        protected extern static bool MoveWindow(
            IntPtr hWnd,
            int X,
            int Y,
            int cx,
            int cy,
            bool repaint);

        [DllImport("User32.dll")]
        protected extern static bool SetWindowPos(
            IntPtr hWnd,
            IntPtr hWndInsertAfter,
            int X,
            int Y,
            int cx,
            int cy,
            uint uFlags);

        [DllImport("User32.dll")]
        protected extern static IntPtr CreateWindowEx(
            int dwExStyle,
            string lpClassName,
            string lpWindowName,
            uint dwStyle,
            int x,
            int y,
            int nWidth,
            int nHeight,
            IntPtr hWndParent,
            IntPtr hMenu,
            IntPtr hInstance,
            IntPtr lpParam);

        [DllImport("User32.dll")]
        protected extern static IntPtr DestroyWindow(IntPtr hWnd);
        public CRenderTarget(int iWidth, int iHeight)
        {
            m_iHeight = iHeight;
            m_iWidth = iWidth;

            return;
        }
        private CRenderTarget()
        {
            return;
        }
        protected override HandleRef BuildWindowCore(HandleRef hParentWnd)
        {
            m_hWnd = CreateWindowEx( 
                0,
                "static",
                "RenderTarget",
                WS_CHILD | WS_VISIBLE | 0,
                0,
                0,
                m_iWidth,
                m_iHeight,
                hParentWnd.Handle,
                IntPtr.Zero,
                IntPtr.Zero,
                IntPtr.Zero);

            return new HandleRef(this, m_hWnd);
        }
        protected override IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            handled = true;

            if(m_WndProc != null)
                return m_WndProc(hWnd, msg, wParam, lParam);

            return IntPtr.Zero;
        }
        protected override void DestroyWindowCore(HandleRef hWnd)
        {
            DestroyWindow(m_hWnd);
        }
    }

    public partial class MainWindow : Window
    {
        [StructLayout(LayoutKind.Sequential)]
        public struct Message
        {
            public IntPtr hWnd;
            public uint Msg;
            public IntPtr wParam;
            public IntPtr lParam;
            public uint Time;
            public Point Point;
        }

        [DllImport("User32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        protected extern static bool PeekMessage(
              ref Message lpMsg,
              IntPtr hWnd,
              uint wMsgFilterMin,
              uint wMsgFilterMax,
              uint wRemoveMsg);

        [DllImport("User32.dll")]
        protected extern static bool TranslateMessage(ref Message msg);

        [DllImport("User32.dll")]
        protected extern static IntPtr DispatchMessage(ref Message msg);

        private static CRenderTarget sm_RenderTarget = null;
        private static CEditor_Wrapper sm_EditorWrapper = null; 
        public MainWindow()
        {
            sm_EditorWrapper = new CEditor_Wrapper();

            InitializeComponent();
        }

        private void MAIN_WINDOW_Loaded(object sender, RoutedEventArgs e)
        {
            sm_RenderTarget = new CRenderTarget((int)RENDER_TARGET.ActualWidth, (int)RENDER_TARGET.ActualHeight);
            RENDER_TARGET.Child = sm_RenderTarget;

            Point p = new Point(3, 3);
            PresentationSource source = PresentationSource.FromVisual(RENDER_TARGET);
            p = source.CompositionTarget.TransformFromDevice.Transform(p);
            p = RENDER_TARGET.PointToScreen(p);

            if(!sm_EditorWrapper.Init(sm_RenderTarget.hWnd, (uint)RENDER_TARGET.ActualWidth, (uint)RENDER_TARGET.ActualHeight))
            {
                this.Close();
            }

            sm_RenderTarget.WndProcPointer = sm_EditorWrapper.WndProc;
        }

        private void MAIN_WINDOW_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if(sm_EditorWrapper != null)
                sm_EditorWrapper.Resize((uint)RENDER_TARGET.ActualWidth, (uint)RENDER_TARGET.ActualHeight);
        }

        private void MAIN_WINDOW_LocationChanged(object sender, EventArgs e)
        {
        }

        [DllImport("User32.dll")]
        protected extern static void PostQuitMessage(int nExitCode);

        private void MAIN_WINDOW_Closed(object sender, EventArgs e)
        {
            PostQuitMessage(0);
        }
    }
}
